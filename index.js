var https = require('https');
var http = require('http');
var url = require('url');
var fs = require('fs');
var spawn = require('child_process').spawn;
var PDFDocument = require('pdfkit');
var concat = require("concat-stream");
var tls = require('tls');

var privateKey = fs.readFileSync('cert/printer.clico.me.key').toString();
var certificate = fs.readFileSync('cert/printer.clico.me-bundle.crt').toString();

var options = {
   key  : privateKey,
   cert : certificate
};


https.createServer(options, (req, res) => {

  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  var query = url.parse(req.url, true).query;

    var doc = new PDFDocument({
            size: [230,230],
            margins: 1,
            print_media_type: true,
            dpi: 200
        });

    doc.pipe(fs.createWriteStream('output.pdf'));

    doc.pipe(concat(function (data) {
        var lpr = spawn('lpr');
        lpr.stdin.end(data);
        lpr.on('exit', function(code) {
          if(code == 0) {
            console.log('print success!');
            res.writeHead(200);
          } else {
            console.log('print fail!');
            res.writeHead(500);
          }
          res.end();
        });

    }));

    doc.rotate(0);

    if(query["NAME"] || query["ORGNAME"]){

      if(query["NAME"] != undefined && query["NAME"].length>15){
        //Name Campusero Trimmed
        doc.font(__dirname + '/Helvetica-Bold.ttf').fillColor("#000000").fontSize(13).text(query["NAME"].substring(0, 12)+"...", 25, 20, {width:230,align:'left'});
      }else{
        //Name Campusero
        doc.font(__dirname + '/Helvetica-Bold.ttf').fillColor("#000000").fontSize(13).text(query["NAME"], 25, 20, {width:230,align:'left'});
      }

      //Verify size of org name
      if(query["ORGNAME"] != undefined && query["ORGNAME"].length>17){
        //Org of Campusero Trimmed
        doc.font(__dirname + '/Helvetica-Bold.ttf').fillColor("#000000").fontSize(13).text(query["ORGNAME"].substring(0, 13)+"...", 25, 36, {width:230,align:'left'});
      }else{
        //Org of Campusero
        doc.font(__dirname + '/Helvetica-Bold.ttf').fillColor("#000000").fontSize(13).text(query["ORGNAME"], 25, 36, {width:230,align:'left'});
      }

      //Barcode
      if(query["BARCODE"] != undefined){
        //Barcode
        doc.font(__dirname + '/3OF9_NEW.TTF').fontSize(20).text('*'+query["BARCODE"]+'*', 25, 55, {width:230,align:'left'});
        
        //Number
        doc.font(__dirname + '/Helvetica-Bold.ttf').fontSize(6).text('*'+query["BARCODE"]+'*', 25, 73, {width:230,align:'left', characterSpacing:9});
      }
      
      //Document Campusero
      if(query["DOC"] != undefined){
        doc.font(__dirname + '/Helvetica-Bold.ttf').fontSize(11).text(query["DOC"], 25, 6, {width:230,align:'left'});  
      }

      //Credencial code
      if(query["COD"] != undefined){
        doc.font(__dirname + '/Helvetica-Bold.ttf').fillColor("#000000").fontSize(10).text(query["COD"], 168, 37, {width:35,align:'left'});
      }

      //Camping Image
      if(query["CAMPING"] != undefined && parseFloat(query["CAMPING"]) != 0){
        doc.image('imgs/camping.png', 164, 0, {width: 38, align:'right'});
      }
      
      doc.end();
    } else {
      res.end();
    }
}).listen(8080);


console.log('Server running on port 8080');
